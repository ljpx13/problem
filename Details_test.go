package problem

import (
	"fmt"
	"testing"

	"gitlab.com/ljpx13/test"
	"gitlab.com/ljpx13/test/verbs/is"
)

func TestDetailsAttachError(t *testing.T) {
	// Arrange.
	problem := &Details{}

	// Act.
	problem.AttachError(fmt.Errorf("an error"))

	// Assert.
	test.That(t, problem.Error, is.EqualTo("an error"))
}

func TestDetailsAttachErrorNil(t *testing.T) {
	// Arrange.
	problem := &Details{
		Error: "an error",
	}

	// Act.
	problem.AttachError(nil)

	// Assert.
	test.That(t, problem.Error, is.EqualTo(""))
}
